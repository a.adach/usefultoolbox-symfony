<?php

declare(strict_types=1);

namespace Adachsoft\UsefultoolboxBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle as BaseBundle;

class UsefultoolboxSymfonyBundle extends BaseBundle
{
    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return __NAMESPACE__;
    }
}
